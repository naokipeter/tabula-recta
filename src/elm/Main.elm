module Main exposing (Flags, Model, Msg(..), init, main, subscriptions, update, view)

-- Imports ---------------------------------------------------------------------

import Browser
import Svg.Styled exposing (Attribute, Svg, rect, svg, text, text_)
import Svg.Styled.Attributes exposing (..)
import Svg.Styled.Events exposing (onClick)



-- Main ------------------------------------------------------------------------


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view >> Svg.Styled.toUnstyled
        , subscriptions = subscriptions
        }



-- Model -----------------------------------------------------------------------


type alias Flags =
    ()


type Highlight
    = HighlightCell Int Int
    | HighlightRow Int
    | HighlightCol Int


type alias Model =
    { highlight : Maybe Highlight
    }


init : Flags -> ( Model, Cmd Msg )
init _ =
    ( { highlight = Nothing
      }
    , Cmd.none
    )



-- Update ----------------------------------------------------------------------


type Msg
    = SelectedRow Int
    | SelectedCol Int
    | SelectedCell Int Int


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SelectedRow row ->
            let
                newHighlight =
                    case model.highlight of
                        Nothing ->
                            Just (HighlightRow row)

                        Just (HighlightRow _) ->
                            Just (HighlightRow row)

                        Just (HighlightCol col) ->
                            Just (HighlightCell row col)

                        Just (HighlightCell _ col) ->
                            Just (HighlightCell row col)
            in
            ( { model | highlight = newHighlight }, Cmd.none )

        SelectedCol col ->
            let
                newHighlight =
                    case model.highlight of
                        Nothing ->
                            Just (HighlightCol col)

                        Just (HighlightRow row) ->
                            Just (HighlightCell row col)

                        Just (HighlightCol _) ->
                            Just (HighlightCol col)

                        Just (HighlightCell row _) ->
                            Just (HighlightCell row col)
            in
            ( { model | highlight = newHighlight }, Cmd.none )

        SelectedCell row col ->
            ( { model | highlight = Just (HighlightCell row col) }, Cmd.none )



-- View ------------------------------------------------------------------------


cellSide =
    20


computeViewbox : Attribute msg
computeViewbox =
    [ -2, -2, 29, 29 ]
        |> List.map ((*) cellSide)
        |> List.map String.fromInt
        |> String.join " "
        |> viewBox


view model =
    svg
        [ computeViewbox
        , fontSize "1em"
        , width "100%"
        , height "100%"
        ]
    <|
        viewTabulaRecta model.highlight


viewTabulaRecta : Maybe Highlight -> List (Svg Msg)
viewTabulaRecta maybeHighlight =
    viewHighlight maybeHighlight
        ++ viewHeaderTop
        ++ viewHeaderLeft
        ++ viewGrid


viewHighlight : Maybe Highlight -> List (Svg Msg)
viewHighlight maybeHighlight =
    case maybeHighlight of
        Nothing ->
            []

        Just (HighlightRow row) ->
            [ rect
                [ x <| String.fromInt <| cellSide * -1
                , y <| String.fromInt <| cellSide * row
                , width <| String.fromInt <| 27 * cellSide
                , height <| String.fromInt <| cellSide
                , style "fill: lightblue; stroke: darkgray; stroke-width:1; fill-opacity:1;"
                ]
                []
            ]

        Just (HighlightCol col) ->
            [ rect
                [ x <| String.fromInt <| cellSide * col
                , y <| String.fromInt <| cellSide * -1
                , width <| String.fromInt <| cellSide
                , height <| String.fromInt <| 27 * cellSide
                , style "fill: gold; stroke: darkgray; stroke-width:1; fill-opacity:1;"
                ]
                []
            ]

        Just (HighlightCell row col) ->
            [ rect
                [ x <| String.fromInt <| cellSide * -1
                , y <| String.fromInt <| cellSide * row
                , width <| String.fromInt <| cellSide * (col + 1)
                , height <| String.fromInt <| cellSide
                , style "fill: lightblue; stroke: darkgray; stroke-width:1; fill-opacity:1;"
                ]
                []
            , rect
                [ x <| String.fromInt <| cellSide * col
                , y <| String.fromInt <| cellSide * row
                , width <| String.fromInt <| cellSide
                , height <| String.fromInt <| cellSide
                , style "fill: limegreen; stroke: darkgray; stroke-width:1; fill-opacity:1;"
                ]
                []
            , rect
                [ x <| String.fromInt <| cellSide * (col + 1)
                , y <| String.fromInt <| cellSide * row
                , width <| String.fromInt <| (26 - col - 1) * cellSide
                , height <| String.fromInt <| cellSide
                , style "fill: lightblue; stroke: darkgray; stroke-width:1; fill-opacity:1;"
                ]
                []
            , rect
                [ x <| String.fromInt <| cellSide * col
                , y <| String.fromInt <| cellSide * -1
                , width <| String.fromInt <| cellSide
                , height <| String.fromInt <| cellSide * (row + 1)
                , style "fill: gold; stroke: darkgray; stroke-width:1; fill-opacity:1;"
                ]
                []
            , rect
                [ x <| String.fromInt <| cellSide * col
                , y <| String.fromInt <| cellSide * (row + 1)
                , width <| String.fromInt <| cellSide
                , height <| String.fromInt <| cellSide * (26 - row - 1)
                , style "fill: gold; stroke: darkgray; stroke-width:1; fill-opacity:1;"
                ]
                []
            ]


viewHeaderTop : List (Svg Msg)
viewHeaderTop =
    List.range 0 25
        |> List.map (\row -> viewHeaderLetter -1 row (SelectedRow row))
        |> List.concat


viewHeaderLeft : List (Svg Msg)
viewHeaderLeft =
    List.range 0 25
        |> List.map (\col -> viewHeaderLetter col -1 (SelectedCol col))
        |> List.concat


viewGrid : List (Svg Msg)
viewGrid =
    List.range 0 25
        |> List.map viewRow
        |> List.concat


viewRow : Int -> List (Svg Msg)
viewRow row =
    List.range 0 25
        |> List.map (\col -> viewLetter row col)
        |> List.concat


viewHeaderLetter : Int -> Int -> Msg -> List (Svg Msg)
viewHeaderLetter row col msg =
    let
        letterTransformation =
            if col == -1 then
                String.toLower

            else
                String.toUpper
    in
    [ text_
        [ x <| String.fromFloat <| toFloat (cellSide * row) + (0.5 * cellSide)
        , y <| String.fromFloat <| toFloat (cellSide * col) + (0.5 * cellSide)
        , width <| String.fromInt cellSide
        , height <| String.fromInt cellSide
        , onClick msg
        , textAnchor "middle"
        , alignmentBaseline "middle"
        , style "cursor: pointer; font-size: 12pt; user-select: none;"
        ]
        [ text <| letterTransformation <| String.fromChar <| Char.fromCode (modBy 26 (row + col + 1) + 97) ]
    ]


viewLetter : Int -> Int -> List (Svg Msg)
viewLetter row col =
    [ text_
        [ x <| String.fromFloat <| toFloat (cellSide * row) + (0.5 * cellSide)
        , y <| String.fromFloat <| toFloat (cellSide * col) + (0.5 * cellSide)
        , width <| String.fromInt cellSide
        , height <| String.fromInt cellSide
        , textAnchor "middle"
        , alignmentBaseline "middle"
        , style "font-size: 12pt; user-select: none;"
        ]
        [ text <| String.fromChar <| Char.fromCode (modBy 26 (row + col) + 65) ]
    , rect
        [ x <| String.fromInt <| cellSide * row
        , y <| String.fromInt <| cellSide * col
        , width <| String.fromInt <| cellSide
        , height <| String.fromInt <| cellSide
        , pointerEvents "visible"
        , onClick (SelectedCell col row)
        , style "cursor: pointer; fill: none; stroke: darkgray; stroke-width:1; fill-opacity:1;"
        ]
        []
    ]



-- Subscriptions ---------------------------------------------------------------


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
